<?php

namespace App;

use App\DTO\FileDto;
use App\Illuminate\StorageParams;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class Exporter
{
    protected StorageParams $storageParams;

    public function __construct(StorageParams $storageParams, string $filePath)
    {
        $this->storageParams = $storageParams;
    }

    public function export(): array
    {
        $displayFilter = (int)$this->storageParams->params->get('number_of_points');
        $multiply = $this->storageParams->params->get('_multiply_export');

        $all = $this->storageParams->data;

        $result = [];
        if ($multiply === true) {
            foreach ($all as $recordId => $data) {
                $result[$recordId] = $this->getResult(collect($data), $displayFilter);
            }
        } else {
            $result = $this->getResult($all, $displayFilter);
        }

        return ['data' => $result];
    }

    private function getResult(Collection $data, int $displayFilter): array
    {
        $counter = 0;
        $firstPoint = false;
        $result = collect();
        foreach ($data as &$item) {
            ++$counter;

            if ($displayFilter > 1 && $counter <= $displayFilter) {
                continue;
            }

            $item['date_time_utc'] =Carbon::parse($item['date_time'])->addHours($item['utc_diff'])->format('Y-m-d H:i:s');
            $item['date_time'] = Carbon::parse($item['date_time'])->format('Y-m-d H:i:s');

            if (!$firstPoint) {
                $item['first_point'] = true;
                $firstPoint = true;
            }

            unset($item['id'], $item['record_id'], $item['record_import_id']);
            $result->push($item);

            $counter = 0;
        }

        return $result->toArray();
    }
}
