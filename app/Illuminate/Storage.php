<?php

namespace App\Illuminate;

use DirectoryIterator;
use JsonMachine\Exception\InvalidArgumentException;
use JsonMachine\Items;

class Storage
{
    /**
     * @throws InvalidArgumentException
     */
    public static function readJson(string $path): array
    {
//        foreach (Items::fromFile($path) as $key => $item) {
//            $result[$key] = $item;
//        }
        return json_decode(file_get_contents($path), true);
    }

    /**
     * @throws \JsonException
     */
    public static function writeJson(string $destinationPath, array $data): false|int
    {
        return file_put_contents($destinationPath, json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE));
    }

    public static function deleteAllFilesFromDir(string $dir, bool $reCreate = false): void
    {
        if ($reCreate && !is_dir($dir) && !mkdir($dir) ) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }

        foreach (new DirectoryIterator($dir) as $fileInfo) {
            if(!$fileInfo->isDot() && $fileInfo->isFile()) {
                unlink($fileInfo->getPathname());
            }
        }

    }
}
